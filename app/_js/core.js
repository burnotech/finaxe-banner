var counter = document.getElementById('counter'),
    count = parseInt(counter.innerHTML.replace('&nbsp;',''));

(function() {
    var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
        window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
    window.requestAnimationFrame = requestAnimationFrame;
})();

function increment() {
    if (count < 600000) {
        count+=10000;
        // console.log(count);
        counter.innerHTML=count/1000+'&nbsp;000';
        requestAnimationFrame(increment);
    } else {
        return false;
    }
}

setTimeout(function () {
    increment();
},1300);

setInterval(function () {
    counter.innerHTML = '200&nbsp;000';
    count = 200000;
    setTimeout(function () {
        increment();
    },1300);
},10000)
