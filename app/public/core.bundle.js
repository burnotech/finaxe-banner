(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
var counter = document.getElementById('counter'),
    count = parseInt(counter.innerHTML.replace('&nbsp;',''));

(function() {
    var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
        window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
    window.requestAnimationFrame = requestAnimationFrame;
})();

function increment() {
    if (count < 600000) {
        count+=10000;
        // console.log(count);
        counter.innerHTML=count/1000+'&nbsp;000';
        requestAnimationFrame(increment);
    } else {
        return false;
    }
}

setTimeout(function () {
    increment();
},1300);

setInterval(function () {
    counter.innerHTML = '200&nbsp;000';
    count = 200000;
    setTimeout(function () {
        increment();
    },1300);
},10000)

},{}]},{},[1]);
